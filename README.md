# Scoreboard

## Requirements

- “Scoreboard service” which shows the high score of a game 
- Page that lists scores. The page must list the name of the player, as well as the amount of “points” that was scored by the player.
- The list must be sortable by the score. 
- At the end of the list, there should be a way for a user to input more scores into the list.

## UI

![list](images/scoreboard-1.png)
![popup](images/scoreboard-2.png)

## List of used tecnologies and libraries:

- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [Materialize](https://materializecss.com/)
- [faker](http://marak.github.io/faker.js/)
- the project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to build and run

In the project directory run commands:

`yarn install`
 
`yarn build`

`yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.