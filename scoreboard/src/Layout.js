import React from 'react';
import { SortOptions, SortedScores, AddScore } from './containers';

const Layout = () => (
    <div className="container">
        <div className="row">
            <div className="col s3">
                <SortOptions/>
            </div>
        </div>
        <div className="row">
            <div className="col s12">
                <SortedScores/>
            </div>
        </div>
        <div className="row right-align">
            <AddScore/>
        </div>
    </div>
);

export default Layout;