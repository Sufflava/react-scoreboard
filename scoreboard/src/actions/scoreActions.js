import faker from 'faker';
import { FETCH_SCORE_LIST, ADD_SCORE } from './actionTypes';

function generateFakeData() {
    const count = 10;
    let scoreList = [];
    for(let i = 0; i < count; i++) {
        const score = {
            id: faker.random.uuid(),
            name: faker.name.findName(),
            score: faker.random.number({ min: 1, max: 100 })
        };
        scoreList.push(score);
    }
    return scoreList;
}

export function fetchScores() {
    return { type: FETCH_SCORE_LIST, data: generateFakeData() };
}

export function addScore(score) {
    return { type: ADD_SCORE, score: { ...score, id: faker.random.uuid() } };
}