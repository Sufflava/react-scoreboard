import { SET_SORT_DIRECTION } from './actionTypes';

const SortDirection = {
    ASC: 'ASC',
    DESC: 'DESC'
};

function setSortDirection(sortDirection) {
    return { type: SET_SORT_DIRECTION, sortDirection };
}

export { SortDirection, setSortDirection };