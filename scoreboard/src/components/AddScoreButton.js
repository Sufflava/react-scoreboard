import React from 'react';
import PropTypes from 'prop-types';
import M from 'materialize-css';
import texts from '../resources/texts';
import { AddScoreModal, ADD_SCORE_MODAL_ELEMENT_ID } from './AddScoreModal';

class AddScoreButton extends React.Component {
    onAddButtonClick(e) {
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
        const modal = document.getElementById(ADD_SCORE_MODAL_ELEMENT_ID);
        const instance = M.Modal.getInstance(modal);
        instance.open();
    }

    render() {
        return (
            <>
                <a className="waves-effect waves-light btn modal-trigger" href="#addScoreModal" onClick={this.onAddButtonClick}>
                    <i className="material-icons left">add</i>
                    {texts.addScore}
                </a>
                <AddScoreModal onSaveScoreClick={this.props.onSaveScoreClick}/>
            </>
        );
    }
}

AddScoreButton.propTypes = {
    onSaveScoreClick: PropTypes.func.isRequired
};

export default AddScoreButton;