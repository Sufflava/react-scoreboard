import React from 'react';
import PropTypes from 'prop-types';
import texts from '../resources/texts';
import NumberInput from './NumberInput';

const AddScoreForm = ({ name, score, onNameChange, onScoreChange }) => (
    <>
        <div className="row">
            <div className="input-field col s12">
                <input 
                    id="name"
                    type="text"
                    value={name}
                    onChange={onNameChange}
                />
                <label className={ name.length ? 'active' : 'dummy' }>
                    {texts.name}
                </label>
            </div>
        </div>
        <div className="row">
            <div className="input-field col s12">  
                <NumberInput value={score} onChange={onScoreChange} id="score" /> 
                <label className="active">{texts.score}</label>
            </div>
        </div>
    </>
);

AddScoreForm.propTypes = {
    name: PropTypes.string.isRequired,
    score: PropTypes.string.isRequired,
    onNameChange: PropTypes.func.isRequired,
    onScoreChange: PropTypes.func.isRequired
};

export default AddScoreForm;