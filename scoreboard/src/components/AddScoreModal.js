import React from 'react';
import PropTypes from 'prop-types';
import M from 'materialize-css';
import texts from '../resources/texts';
import AddScoreForm from './AddScoreForm';

const ADD_SCORE_MODAL_ELEMENT_ID = 'addScoreModal';

class AddScoreModal extends React.Component {       
    constructor(props) {
        super(props);
        this.state = { 
            newScore: { name: '', score: '' } 
        };

        this.onNameChange = this.onNameChange.bind(this);
        this.onScoreChange = this.onScoreChange.bind(this);

        this.onSaveButtonClick = this.onSaveButtonClick.bind(this);
        this.onCloseButtonClick = this.onCloseButtonClick.bind(this);
    }    

    componentDidMount() {
        document.addEventListener('DOMContentLoaded', function() {
            const modal = document.getElementById(ADD_SCORE_MODAL_ELEMENT_ID);
            M.Modal.init(modal);
        });
    }

    onNameChange(e) {
        this.setState({
            newScore: {
                ...this.state.newScore,
                name: e.target.value
            }
        });
    }

    onScoreChange(e) {
        this.setState({
            newScore: {
                ...this.state.newScore,
                score: e.target.value
            }
        });
    }

    closeModal() {
        const modal = document.getElementById(ADD_SCORE_MODAL_ELEMENT_ID);
        const instance = M.Modal.getInstance(modal);
        instance.close();

        this.setState({ 
            newScore: { name: '', score: '' } 
        });
    }

    onSaveButtonClick(e) {
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
        const newScore = { ...this.state.newScore, score: Number(this.state.newScore.score) };
        this.props.onSaveScoreClick(newScore);
        this.closeModal();
    }

    onCloseButtonClick(e) {
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
        this.closeModal();
    }

    render() {
        const { name, score } = this.state.newScore;

        return (
            <div id={ADD_SCORE_MODAL_ELEMENT_ID} className="modal left-align">
                <div className="modal-content">
                    <h4>{texts.addNewScore}</h4>
                    <AddScoreForm 
                        name={name} 
                        score={score} 
                        onNameChange={this.onNameChange} 
                        onScoreChange={this.onScoreChange}
                    />
                </div>
                <div className="modal-footer right-align">
                    <a 
                        href="#!" 
                        className="modal-close btn-flat waves-effect waves-light-green" 
                        onClick={this.onCloseButtonClick}
                    >
                        {texts.close}
                    </a>
                    <a 
                        href="#!" 
                        className="modal-action modal-close btn waves-effect waves-light" 
                        onClick={this.onSaveButtonClick}
                        disabled={!name.length}
                    >
                        {texts.addScore}
                    </a>
                </div>
            </div>
        );
    }
}
  
AddScoreModal.propTypes = {
    onSaveScoreClick: PropTypes.func.isRequired
};

export { AddScoreModal, ADD_SCORE_MODAL_ELEMENT_ID };