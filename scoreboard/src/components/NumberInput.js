import React from 'react';
import PropTypes from 'prop-types';

const NUMBER_REGEX = /^[0-9\b]+$/;

class NumberInput extends React.Component {
    constructor() { 
        super();
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        const value = e.target.value;
        if (value === '' || NUMBER_REGEX.test(value)) {
            this.props.onChange(e);
        } else {
            this.props.onChange({ target: { ...e.target, value: '' }});
        }
    }
      
    render() {
        return (
            <input 
                id={this.props.id}
                type="text"
                value={this.props.value}
                onChange={this.onChange}
            />
        );
    }
}

NumberInput.propTypes = {
    id: PropTypes.string,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};

export default NumberInput;