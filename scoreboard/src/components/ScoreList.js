import React from 'react';
import PropTypes from 'prop-types';
import texts from '../resources/texts';
import ScoreListItem from './ScoreListItem';

class ScoreList extends React.Component {
    componentDidMount() {
        this.props.scoreActions.fetchScores();
    }
      
    render() {
        return (
            <table className="striped">
                <thead>
                    <tr>
                        <th>{texts.name}</th>
                        <th>{texts.score}</th>
                    </tr>
                </thead>

                <tbody>
                    {this.props.scores.map(s => (
                        <ScoreListItem key={s.id} name={s.name} score={s.score} />
                    ))}
                </tbody>
            </table>
        );
    }
}

ScoreList.propTypes = {
    scores: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            score: PropTypes.number.isRequired 
        }).isRequired
    ).isRequired,
    scoreActions: PropTypes.object
};

export default ScoreList;