import React from 'react';
import PropTypes from 'prop-types';

const ScoreListItem = ({ name, score }) => (
    <tr>
        <td>{name}</td>
        <td>{score}</td>
    </tr>
);

ScoreListItem.propTypes = {
    name: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired
};

export default ScoreListItem;