import React from 'react';
import PropTypes from 'prop-types';
import M from 'materialize-css';
import texts from '../resources/texts';
import { SortDirection } from '../actions';

const DROPDOWN_ELEMENT_ID = 'sortDropdown';

class SortDropdown extends React.Component {
    constructor() { 
        super();      
        this.onValueChange = this.onValueChange.bind(this);
    }

    componentDidMount() {
        document.addEventListener('DOMContentLoaded', function() {
            let select = document.getElementById(DROPDOWN_ELEMENT_ID);
            M.FormSelect.init(select);
        });
    }
  
    onValueChange(e) {
        this.props.onChange(e.target.value);
    }
        
    render() {
        return (
            <div className="input-field col s12">
                <select value={this.props.value} onChange={this.onValueChange} id={DROPDOWN_ELEMENT_ID}>
                    <option value={SortDirection.ASC}>{texts.asc}</option>
                    <option value={SortDirection.DESC}>{texts.desc}</option>
                </select>
                <label>{texts.scoreOrder}</label>
            </div>
        );
    }
}

SortDropdown.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
}; 

export default SortDropdown;