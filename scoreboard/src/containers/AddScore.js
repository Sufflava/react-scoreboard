import { connect } from 'react-redux';
import { addScore } from '../actions';
import AddScoreButton from '../components/AddScoreButton';

const mapDispatchToProps = (dispatch) => {
    return {
        onSaveScoreClick: (newScore) => {
            dispatch(addScore(newScore));
        }
    };
};

const AddScore = connect(null, mapDispatchToProps)(AddScoreButton);

export default AddScore;