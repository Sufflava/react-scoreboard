import { connect } from 'react-redux';
import { setSortDirection } from '../actions';
import SortDropdown from '../components/SortDropdown';

const mapStateToProps = (state) => {
    return {
        value: state.sortDirection
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChange: (sortDirection) => {
            dispatch(setSortDirection(sortDirection));
        }
    };
};

const SortOptions = connect(mapStateToProps, mapDispatchToProps)(SortDropdown);

export default SortOptions;