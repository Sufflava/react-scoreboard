import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SortDirection } from '../actions';
import * as scoreActions from '../actions/scoreActions';
import ScoreList from '../components/ScoreList';

const compareScores = (a, b) => a.score - b.score;

const getSortedScores = (scores, sortDirection) => {
    let result = [...scores];
    result.sort(compareScores);
  
    if (sortDirection === SortDirection.DESC) {
        result.reverse();
    }

    return result;
};

const mapStateToProps = state => {
    return {
        scores: getSortedScores(state.scores, state.sortDirection)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        scoreActions: bindActionCreators(scoreActions, dispatch)
    };
};

const SortedScores = connect(mapStateToProps, mapDispatchToProps)(ScoreList);

export default SortedScores;