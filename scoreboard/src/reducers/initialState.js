import { SortDirection } from '../actions';

export default {
    scores: [],
    sortDirection: SortDirection.ASC
};
