import { combineReducers } from 'redux';
import scoresReducer from './scoresReducer';
import sortDirectionReducer from './sortDirectionReducer';

const rootReducer = combineReducers({
    scores: scoresReducer,
    sortDirection: sortDirectionReducer
});

export default rootReducer;