import { FETCH_SCORE_LIST, ADD_SCORE } from '../actions';
import initialState from './initialState';

function scoresReducer(state = initialState.scores, action) {
    switch (action.type) {
    case FETCH_SCORE_LIST:
        return [...action.data];
    case ADD_SCORE:
        return [
            ...state,
            action.score
        ];
    default:
        return state;
    }
}

export default scoresReducer;