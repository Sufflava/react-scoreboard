import { SET_SORT_DIRECTION } from '../actions';
import initialState from './initialState';


function sortDirectionReducer(state = initialState.sortDirection, action) {
    switch (action.type) {
    case SET_SORT_DIRECTION:
        return action.sortDirection;
    default:
        return state;
    }
}

export default sortDirectionReducer;