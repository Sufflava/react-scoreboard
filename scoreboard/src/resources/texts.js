const texts = {
    score: 'Score',
    name: 'Name',
    addScore: 'Add score',
    addNewScore: 'Add new score',
    scoreOrder: 'Score order',
    close: 'Close',
    asc: 'Asc',
    desc: 'Desc'
};

export default texts;